package com.example.dashboard;

import com.example.dashboard.entity.*;
import com.example.dashboard.enums.Anstellung;
import com.example.dashboard.enums.FachEnums;
import com.example.dashboard.enums.GruppeEnums;
import com.example.dashboard.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DashboardApplication implements CommandLineRunner {

	@Autowired
	TrainerInCRUDRepository trainerInCRUDRepository;

	@Autowired
	TeilnehmerInCRUDRepository teilnehmerInCRUDRepository;

	@Autowired
	GruppeCRUDRepository gruppeCRUDRepository;

	@Autowired
	FachCRUDRepository fachCRUDRepository;

	@Autowired
	RaumCRUDRepository raumCRUDRepository;

	public static void main(String[] args) {
		SpringApplication.run(DashboardApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		try {
			raumCRUDRepository.save(new Raum("Ada Lovelace",5));
			raumCRUDRepository.save(new Raum("AJohn Atanasoff",7));
			//gruppeCRUDRepository.save(new Gruppe(GruppeEnums.SW, 1, false));
			fachCRUDRepository.save(new Fach(FachEnums.JAVA, "Spring Boot"));
			fachCRUDRepository.save(new Fach(FachEnums.WEB, "Vue"));
			fachCRUDRepository.save(new Fach(FachEnums.JAVA, "JPA"));
			fachCRUDRepository.save(new Fach(FachEnums.WEB, "AXIOS"));
			fachCRUDRepository.save(new Fach(FachEnums.DB, "JOINS"));

		} catch (Exception e) {
			System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage());
		}


		for (int i = 0; i < 8; i++) {
			String vorname = "";
			String nachname = "";
			String raum = "";

			int zufallVorname = (int) (Math.random() * 11);
			int zufallNachname = (int) (Math.random() * 11);
			int zufallRaum = (int) (Math.random() * 13);
			int zufallTelefonnummer = (int)(Math.random()*10000000);

			raum = "Raum " + zufallRaum;

			switch (zufallVorname) {
				case 0:
					vorname = "Emma";
					break;
				case 1:
					vorname = "Michael";
					break;
				case 2:
					vorname = "Marco";
					break;
				case 3:
					vorname = "Sara";
					break;
				case 4:
					vorname = "Lisa";
					break;
				case 5:
					vorname = "Peter";
					break;
				case 6:
					vorname = "Moritz";
					break;
				case 7:
					vorname = "Felix";
					break;
				case 8:
					vorname = "Susanne";
					break;
				case 9:
					vorname = "Mia";
					break;
				case 10:
					vorname = "Simon";
					break;
			}

			switch (zufallNachname) {
				case 0:
					nachname = "Mustermann";
					break;
				case 1:
					nachname = "Musterfrau";
					break;
				case 2:
					nachname = "Müller";
					break;
				case 3:
					nachname = "Rosenrot";
					break;
				case 4:
					nachname = "Bauer";
					break;
				case 5:
					nachname = "Weber";
					break;
				case 6:
					nachname = "Hoffmann";
					break;
				case 7:
					nachname = "Hemmer";
					break;
				case 8:
					nachname = "Leithner";
					break;
				case 9:
					nachname = "Brunner";
					break;
				case 10:
					nachname = "Vogel";
					break;
			}

			String telefonnummer = "+43 664 " + zufallTelefonnummer;
			String name = vorname + " " + nachname;
			String emailadresse = vorname.toLowerCase() + "." + nachname.toLowerCase() + "@outlook.com";


			try {
				trainerInCRUDRepository.save(new TrainerIn(name, emailadresse, telefonnummer, "Mustergasse", Anstellung.VOLLZEIT, 3500, FachEnums.JAVA));
				teilnehmerInCRUDRepository.save(new TeilnehmerIn(name, emailadresse, telefonnummer, "TeilnehmerAdresse", "10.10.2022", "10.04.2023" ));
				//gruppeCRUDRepository.save(new Gruppe(GruppeEnums.SW, 1, false));
				//fachCRUDRepository.save(new Fach(FachEnums.JAVA, "Java Bezeichnung"));

			} catch (Exception e) {
				System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage());
			}

		}

	}
}