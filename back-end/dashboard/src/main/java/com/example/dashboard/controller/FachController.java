package com.example.dashboard.controller;


import com.example.dashboard.entity.Fach;
import com.example.dashboard.repository.FachCRUDRepository;
import com.example.dashboard.services.FachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FachController {

    @Autowired
    FachService fachService;

    @CrossOrigin
    @GetMapping("/fach/allFaecher")
    public List getFaecher() {
        return fachService.getAllFaecher();
    }

    @CrossOrigin
    @PostMapping("/fachReturn")
    public Integer postFachReturnId(@RequestBody Fach fach) {
        return fachService.postFach(fach);
    }

    @CrossOrigin
    @PutMapping("/fach/{id}")
    public void putFach(@RequestBody Fach fach, @PathVariable int id) {
        fachService.putFach(fach, id);
    }

    @CrossOrigin
    @DeleteMapping("/fach/{fachId}")
    public void deleteFach(@PathVariable int fachId) {
        fachService.deleteFach(fachId);
    }

}
