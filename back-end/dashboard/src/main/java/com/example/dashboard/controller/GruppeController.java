package com.example.dashboard.controller;


import com.example.dashboard.dtos.GruppeDTO;
import com.example.dashboard.dtos.UebersichtDTO;
import com.example.dashboard.entity.Gruppe;
import com.example.dashboard.services.GruppeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GruppeController {

    @Autowired
    GruppeService gruppeService;


    @CrossOrigin
    @PostMapping("/gruppe")
    public void postGruppe(@RequestBody Gruppe gruppe) {
        gruppeService.postGruppe(gruppe);
    }

    @CrossOrigin
    @GetMapping("/gruppe")
    public List getGruppe() {
        return gruppeService.getAllGruppe();
    }

    @CrossOrigin
    @PutMapping("/gruppe/{id}")
    public void putGruppe(@RequestBody GruppeDTO gruppeDTO, @PathVariable int id) {
        gruppeService.putGruppe(gruppeDTO, id);
    }

    @CrossOrigin
    @DeleteMapping("/gruppe/{gruppeId}")
    public void deleteGruppe(@PathVariable int gruppeId) {
        gruppeService.deleteGruppe(gruppeId);
    }

    @CrossOrigin
    @PostMapping("/gruppe/dtos")
    public void setGruppe(@RequestBody GruppeDTO gruppeDTO) {
        gruppeService.setGruppe(gruppeDTO.getArtDerAusbildung(), gruppeDTO.getGruppenNummer(), gruppeDTO.isQualifying(), gruppeDTO.getRaumId());
    }

    @CrossOrigin
    @GetMapping("/gruppe/uebersicht/{gruppeId}")
    public UebersichtDTO getUebersichtDTO(@PathVariable int gruppeId) {
        return gruppeService.kompletteUebersicht(gruppeId);
    }
}
