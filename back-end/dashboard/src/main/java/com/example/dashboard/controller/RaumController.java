package com.example.dashboard.controller;


import com.example.dashboard.dtos.RaumDTO;
import com.example.dashboard.entity.Raum;
import com.example.dashboard.services.RaumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RaumController {

    @Autowired
    RaumService raumService;


    @CrossOrigin
    @PostMapping("/raum")
    public Integer postRaumReturnId(@RequestBody Raum raum) {
       return raumService.postRaum(raum);
    }

    @CrossOrigin
    @GetMapping("/raum")
    public List getRaum() {
        return raumService.getAllRaeume();
    }

    @CrossOrigin
    @PutMapping("/raum/{id}")
    public void putRaum(@RequestBody Raum raum, @PathVariable int id) {
        raumService.putRaum(raum, id);
    }

    @CrossOrigin
    @DeleteMapping("/raum/{raumId}")
    public void deleteRaum(@PathVariable int raumId) {
        raumService.deleteRaum(raumId);
    }

    @CrossOrigin
    @PostMapping("/raum/dtos")
    public void setRaum(@RequestBody RaumDTO raumDTO) {
        raumService.setRaum(raumDTO.getRaumBezeichnung(), raumDTO.getRaumNummer());
    }
}
