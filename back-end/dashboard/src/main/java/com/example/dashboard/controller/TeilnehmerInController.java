package com.example.dashboard.controller;


import com.example.dashboard.dtos.TeilnehmerDTO;
import com.example.dashboard.entity.TeilnehmerIn;
import com.example.dashboard.services.TeilnehmerInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeilnehmerInController {

    @Autowired
    TeilnehmerInService teilnehmerInService;


    @CrossOrigin
    @PostMapping("/teilnehmerIn")
    public void postTeilnehmer(@RequestBody TeilnehmerIn teilnehmerIn) {
        teilnehmerInService.post(teilnehmerIn);
    }

    @CrossOrigin
    @PostMapping("/teilnehmerInReturn")
    public Integer postTeilnehmerReturnId(@RequestBody TeilnehmerIn teilnehmerIn) {
        return teilnehmerInService.post(teilnehmerIn);
    }

    @CrossOrigin
    @GetMapping("/teilnehmerIn/allTeilnehmer")
    public List getTeilnehmer() {
        return teilnehmerInService.getAllTeilnehmer();
    }

    @CrossOrigin
    @GetMapping("/teilnehmerIn/byid/{id}")
    public TeilnehmerIn getTeilnehmerById(@PathVariable int id) {
        return teilnehmerInService.getTeilnehmerById(id);
    }

    @CrossOrigin
    @PutMapping("/teilnehmerIn/{id}")
    public void putTeilnehmer(@RequestBody TeilnehmerDTO teilnehmerDTO, @PathVariable int id) {
        teilnehmerInService.putTeilnehmerIn(teilnehmerDTO, id);
    }

    @CrossOrigin
    @DeleteMapping("/teilnehmerIn/{teilnehmerId}")
    public void deleteTrainer(@PathVariable int teilnehmerId) {
        teilnehmerInService.deleteTeilnehmer(teilnehmerId);
    }

    @CrossOrigin
    @PostMapping("/teilnehmerIn/dtos")
    public void setTeilnehmer(@RequestBody TeilnehmerDTO teilnehmerDTO) {
        teilnehmerInService.setTeilnehmer(teilnehmerDTO.getName(), teilnehmerDTO.geteMailAdresse(), teilnehmerDTO.getTelNummer(), teilnehmerDTO.getAdresse(), teilnehmerDTO.getStartDatum(), teilnehmerDTO.getEndDatum(), teilnehmerDTO.getGruppeId());
    }



}
