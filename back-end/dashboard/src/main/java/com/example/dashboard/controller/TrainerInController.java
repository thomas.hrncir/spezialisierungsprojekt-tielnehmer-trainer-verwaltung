package com.example.dashboard.controller;

import com.example.dashboard.dtos.GetTrainerInDTO;
import com.example.dashboard.dtos.TrainerInDTO;
import com.example.dashboard.entity.TrainerIn;
import com.example.dashboard.services.TrainerInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TrainerInController {

    @Autowired
    TrainerInService trainerInService;


    @CrossOrigin
    @PostMapping("/trainerIn")
    public void postTrainer(@RequestBody TrainerIn trainerIn) {
        trainerInService.post(trainerIn);
    }

    @CrossOrigin
    @PostMapping("/trainerInReturn")
    public Integer postTrainerReturnId(@RequestBody TrainerIn trainerIn) {
        return trainerInService.postId(trainerIn);
    }

    @CrossOrigin
    @GetMapping("/trainerIn/allTrainer")
    public List getTrainer() {
        return trainerInService.getAllTrainer();
    }

    @CrossOrigin
    @GetMapping("/trainerIn/Trainer/{trainerId}")
    public TrainerIn getTrainerByID(@PathVariable int trainerId) {
        return trainerInService.getTrainerById(trainerId);
    }

    @CrossOrigin
    @PutMapping("/trainerIn/{id}")
    public void putTrainer(@RequestBody TrainerInDTO trainerInDTO, @PathVariable int id) {
        trainerInService.putTrainerIn(trainerInDTO, id);
    }

    @CrossOrigin
    @DeleteMapping("/trainerIn/{trainerId}")
    public void deleteTrainer(@PathVariable int trainerId) {
        trainerInService.deleteTrainer(trainerId);
    }

    @CrossOrigin
    @PostMapping("/trainerIn/dtos")
    public void setTrainer(@RequestBody TrainerInDTO trainerInDTO) {
        trainerInService.setTrainer(trainerInDTO.getName(), trainerInDTO.geteMailAdresse(), trainerInDTO.getTelNummer(), trainerInDTO.getAdresse(), trainerInDTO.getAnstellung(), trainerInDTO.getBruttoGehalt(), trainerInDTO.getFachEnums(), trainerInDTO.getFachId());
    }

    @CrossOrigin
    @GetMapping("trainerIn/gehalt/{trainerId}")
    public double getTrainerNettoGehalt(@PathVariable int trainerId) {
        return trainerInService.calculateNettoSalary(trainerId);
    }

    @CrossOrigin
    @GetMapping("/trainerIn/allTrainerDTO")
    public List<GetTrainerInDTO> getAllTrainerDTO() {
        return trainerInService.getAllTrainerDTO();
    }


}
