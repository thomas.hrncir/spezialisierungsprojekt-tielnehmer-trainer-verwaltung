package com.example.dashboard.controller;


import com.example.dashboard.dtos.UnterrichtsTagDTO;
import com.example.dashboard.entity.UnterrichtsTag;
import com.example.dashboard.services.UnterrichtsTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UnterrichtsTagController {

    @Autowired
    UnterrichtsTagService unterrichtsTagService;


    @CrossOrigin
    @PostMapping("/unterrichtsTag")
    public Integer postUnterrichtsTag(@RequestBody UnterrichtsTag unterrichtsTag) {
        return unterrichtsTagService.post(unterrichtsTag);
    }

    @CrossOrigin
    @GetMapping("/unterrichtsTag")
    public List getUnterrichtsTag() {
        return unterrichtsTagService.getAllUnterrichtsTage();
    }

    @CrossOrigin
    @PutMapping("/unterrichtsTag/{id}")
    public void putUnterrichtsTag(@RequestBody UnterrichtsTagDTO unterrichtsTagDTO, @PathVariable int id) {
        unterrichtsTagService.putUnterrichtsTag(unterrichtsTagDTO, id);
    }

    @CrossOrigin
    @DeleteMapping("/unterrichtsTag/{unterrichtsTagId}")
    public void deleteUnterrichtsTag(@PathVariable int unterrichtsTagId) {
        unterrichtsTagService.deleteUnterrichtsTag(unterrichtsTagId);
    }

    @CrossOrigin
    @PostMapping("/unterrichtsTag/dtos")
    public void setUnterrichtsTag(@RequestBody UnterrichtsTagDTO unterrichtsTagDTO) {
        unterrichtsTagService.setUnterricht(unterrichtsTagDTO.getTrainerId(), unterrichtsTagDTO.getGruppeId(), unterrichtsTagDTO.getTageEnums(), unterrichtsTagDTO.getFachId());
    }
}
