package com.example.dashboard.dtos;

import com.example.dashboard.enums.GruppeEnums;

public class GruppeDTO {

    private GruppeEnums artDerAusbildung;
    private int gruppenNummer;
    private boolean qualifying;
    private int raumId;


    public GruppeDTO(GruppeEnums artDerAusbildung, int gruppenNummer, boolean qualifying, int raumId) {
        this.artDerAusbildung = artDerAusbildung;
        this.gruppenNummer = gruppenNummer;
        this.qualifying = qualifying;
        this.raumId = raumId;
    }//TODO Gruppen DTO ohne raum id erstellen

    //getter & setter


    public GruppeEnums getArtDerAusbildung() {
        return artDerAusbildung;
    }

    public void setArtDerAusbildung(GruppeEnums artDerAusbildung) {
        this.artDerAusbildung = artDerAusbildung;
    }

    public int getGruppenNummer() {
        return gruppenNummer;
    }

    public void setGruppenNummer(int gruppenNummer) {
        this.gruppenNummer = gruppenNummer;
    }

    public boolean isQualifying() {
        return qualifying;
    }

    public void setQualifying(boolean qualifying) {
        this.qualifying = qualifying;
    }

    public int getRaumId() {
        return raumId;
    }

    public void setRaumId(int raumId) {
        this.raumId = raumId;
    }
}
