package com.example.dashboard.dtos;

public class RaumDTO {
    private String raumBezeichnung;
    private int raumNummer;
    //private int gruppeId;

    public RaumDTO(String raumBezeichnung, int raumNummer) {
        this.raumBezeichnung = raumBezeichnung;
        this.raumNummer = raumNummer;
        //this.gruppeId = gruppeId;
    }

    // getter & setter


    public String getRaumBezeichnung() {
        return raumBezeichnung;
    }

    public void setRaumBezeichnung(String raumBezeichnung) {
        this.raumBezeichnung = raumBezeichnung;
    }

    public int getRaumNummer() {
        return raumNummer;
    }

    public void setRaumNummer(int raumNummer) {
        this.raumNummer = raumNummer;
    }

//    public int getGruppeId() {
//        return gruppeId;
//    }
//
//    public void setGruppeId(int gruppeId) {
//        this.gruppeId = gruppeId;
//    }
}
