package com.example.dashboard.dtos;

public class TeilnehmerDTO {

    private String name;
    private String eMailAdresse;
    private String telNummer;
    private String adresse;
    private String startDatum;
    private String endDatum;
    private int gruppeId;

    public TeilnehmerDTO() { }

    public TeilnehmerDTO(String name, String eMailAdresse) {
        this.name = name;
        this.eMailAdresse = eMailAdresse;
    }


    public TeilnehmerDTO(String name, String eMailAdresse, String telNummer, String adresse, String startDatum, String endDatum, int gruppeId) {
        this.name = name;
        this.eMailAdresse = eMailAdresse;
        this.telNummer = telNummer;
        this.adresse = adresse;
        this.startDatum = startDatum;
        this.endDatum = endDatum;
        this.gruppeId = gruppeId;
    }

    //getter & setter


    public int getGruppeId() {
        return gruppeId;
    }

    public void setGruppeId(int gruppeId) {
        this.gruppeId = gruppeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteMailAdresse() {
        return eMailAdresse;
    }

    public void seteMailAdresse(String eMailAdresse) {
        this.eMailAdresse = eMailAdresse;
    }

    public String getTelNummer() {
        return telNummer;
    }

    public void setTelNummer(String telNummer) {
        this.telNummer = telNummer;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getStartDatum() {
        return startDatum;
    }

    public void setStartDatum(String startDatum) {
        this.startDatum = startDatum;
    }

    public String getEndDatum() {
        return endDatum;
    }

    public void setEndDatum(String endDatum) {
        this.endDatum = endDatum;
    }
}
