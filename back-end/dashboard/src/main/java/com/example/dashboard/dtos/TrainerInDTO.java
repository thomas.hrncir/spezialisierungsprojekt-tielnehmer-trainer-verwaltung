package com.example.dashboard.dtos;

import com.example.dashboard.enums.Anstellung;
import com.example.dashboard.enums.FachEnums;

public class TrainerInDTO {

    //private int id;
    private String name;
    private String eMailAdresse;
    private String telNummer;
    private String adresse;

    private Anstellung anstellung;
    private float bruttoGehalt;
    private FachEnums fachEnums;

    private int fachId;

    public TrainerInDTO() {}

    public TrainerInDTO(String name, String eMailAdresse, String telNummer, String adresse, Anstellung anstellung, float bruttoGehalt, FachEnums fachEnums, int fachId ) {
        this.name = name;
        this.eMailAdresse = eMailAdresse;
        this.telNummer = telNummer;
        this.adresse = adresse;
        this.anstellung = anstellung;
        this.bruttoGehalt = bruttoGehalt;
        this.fachEnums = fachEnums;
        this.fachId = fachId;
    }

    // getter & setter


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteMailAdresse() {
        return eMailAdresse;
    }

    public void seteMailAdresse(String eMailAdresse) {
        this.eMailAdresse = eMailAdresse;
    }

    public String getTelNummer() {
        return telNummer;
    }

    public void setTelNummer(String telNummer) {
        this.telNummer = telNummer;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Anstellung getAnstellung() {
        return anstellung;
    }

    public void setAnstellung(Anstellung anstellung) {
        this.anstellung = anstellung;
    }

    public float getBruttoGehalt() {
        return bruttoGehalt;
    }

    public void setBruttoGehalt(float bruttoGehalt) {
        this.bruttoGehalt = bruttoGehalt;
    }

    public FachEnums getFachEnums() {
        return fachEnums;
    }

    public void setFachEnums(FachEnums fachEnums) {
        this.fachEnums = fachEnums;
    }

    public int getFachId() {
        return fachId;
    }

    public void setFachId(int fachId) {
        this.fachId = fachId;
    }
}
