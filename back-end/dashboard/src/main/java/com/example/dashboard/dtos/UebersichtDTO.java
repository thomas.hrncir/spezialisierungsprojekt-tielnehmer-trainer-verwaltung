package com.example.dashboard.dtos;

import com.example.dashboard.entity.UnterrichtsTag;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class UebersichtDTO {

    Set<UnterrichtsTag> unterrichtsTag = new HashSet<>();
    Set<TeilnehmerDTO> teilnehmerDTOList = new HashSet<>();


    public UebersichtDTO(Set unterrichtsTag, Set teilnehmerDTOSet) {
        this.unterrichtsTag = unterrichtsTag;
        this.teilnehmerDTOList = teilnehmerDTOSet;
    }

    //getter & setter

    public Set<UnterrichtsTag> getUnterrichtsTag() {
        return unterrichtsTag;
    }

    public void setUnterrichtsTag(Set<UnterrichtsTag> unterrichtsTag) {
        this.unterrichtsTag = unterrichtsTag;
    }


    public Set<TeilnehmerDTO> getTeilnehmerDTOList() {
        return teilnehmerDTOList;
    }

    public void setTeilnehmerDTOList(Set<TeilnehmerDTO> teilnehmerDTOList) {
        this.teilnehmerDTOList = teilnehmerDTOList;
    }
}
