package com.example.dashboard.dtos;

import com.example.dashboard.enums.TageEnums;

public class UnterrichtsTagDTO {
    private TageEnums tageEnums;
    private int trainerId;
    private int fachId;
    private int gruppeId;

    public UnterrichtsTagDTO(int trainerId, int gruppeId, TageEnums tageEnums, int fachId) {
        this.tageEnums = tageEnums;
        this.trainerId = trainerId;
        this.fachId = fachId;
        this.gruppeId = gruppeId;
    }

    //getter & setter

    public TageEnums getTageEnums() {
        return tageEnums;
    }

    public void setTageEnums(TageEnums tageEnums) {
        this.tageEnums = tageEnums;
    }

    public int getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(int trainerId) {
        this.trainerId = trainerId;
    }

    public int getFachId() {
        return fachId;
    }

    public void setFachId(int fachId) {
        this.fachId = fachId;
    }

    public int getGruppeId() {
        return gruppeId;
    }

    public void setGruppeId(int gruppeId) {
        this.gruppeId = gruppeId;
    }
}
