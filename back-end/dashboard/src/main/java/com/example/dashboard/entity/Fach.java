package com.example.dashboard.entity;

import com.example.dashboard.enums.FachEnums;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Fach {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Enumerated(EnumType.STRING)
    private FachEnums fachUnterricht;

    @Column
    private String fachBezeichnung;

    @OneToMany(mappedBy = "fach")
    @JsonBackReference(value = "fachUnterricht")
    private Set<UnterrichtsTag> unterrichtsTagSet = new HashSet<>();

    @OneToMany(mappedBy = "fach")
    @JsonBackReference(value = "fachTrainer")
    private Set<TrainerIn> TrainerInSet = new HashSet<>();

    public Fach() {}

    public Fach(FachEnums fachUnterricht, String fachBezeichnung) {
        this.fachUnterricht = fachUnterricht;
        this.fachBezeichnung = fachBezeichnung;
    }

    //getter & setter

    public void deleteUnterricht(UnterrichtsTag unterrichtsTag) {
        unterrichtsTagSet.remove(unterrichtsTag);
    }

    public void deleteTrainer(TrainerIn trainerIn) {
        TrainerInSet.remove(trainerIn);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FachEnums getFachUnterricht() {
        return fachUnterricht;
    }

    public void setFachUnterricht(FachEnums fachUnterricht) {
        this.fachUnterricht = fachUnterricht;
    }

    public String getFachBezeichnung() {
        return fachBezeichnung;
    }

    public void setFachBezeichnung(String fachBezeichnung) {
        this.fachBezeichnung = fachBezeichnung;
    }

    public Set<UnterrichtsTag> getUnterrichtsTagSet() {
        return unterrichtsTagSet;
    }

    public void setUnterrichtsTagSet(UnterrichtsTag unterrichtsTag) {
        this.unterrichtsTagSet.add(unterrichtsTag);
    }

    public Set<TrainerIn> getTrainerInSet() {
        return TrainerInSet;
    }

    public void setTrainerInSet(TrainerIn trainerIn) {
        this.TrainerInSet.add(trainerIn);
    }
}
