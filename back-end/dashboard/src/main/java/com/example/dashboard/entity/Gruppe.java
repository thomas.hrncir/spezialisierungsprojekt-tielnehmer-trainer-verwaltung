package com.example.dashboard.entity;


import com.example.dashboard.enums.GruppeEnums;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Gruppe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Enumerated(EnumType.STRING)
    private GruppeEnums artDerAusbildung;

    @Column
    private int gruppenNummer;

    @Column
    boolean qualifying;

    @OneToMany(mappedBy = "gruppe")
    @JsonBackReference(value = "gruppeUnterricht")
    private Set<UnterrichtsTag> unterrichtsTagSet = new HashSet<>();

    @OneToMany(mappedBy = "gruppe")
    @JsonManagedReference(value = "teilnehmerGruppe")
    private Set<TeilnehmerIn> TeilnehmerInSet = new HashSet<>();

    @OneToOne(mappedBy = "gruppe")
    private Raum raum;

    public Gruppe() {}

    public Gruppe(GruppeEnums artDerAusbildung, int gruppenNummer, boolean qualifying, Raum raum){
        this.artDerAusbildung = artDerAusbildung;
        this.gruppenNummer = gruppenNummer;
        this.qualifying = qualifying;
        this.raum = raum;
    }

    public Gruppe(GruppeEnums artDerAusbildung, int gruppenNummer, boolean qualifying){
        this.artDerAusbildung = artDerAusbildung;
        this.gruppenNummer = gruppenNummer;
        this.qualifying = qualifying;
    }

    //getter & setter

    public void deleteTeilnehmer(TeilnehmerIn teilnehmerIn) {
        TeilnehmerInSet.remove(teilnehmerIn);
    }

    public void deleteUnterricht(UnterrichtsTag unterrichtsTag) {
        unterrichtsTagSet.remove(unterrichtsTag);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GruppeEnums getArtDerAusbildung() {
        return artDerAusbildung;
    }

    public void setArtDerAusbildung(GruppeEnums artDerAusbildung) {
        this.artDerAusbildung = artDerAusbildung;
    }

    public int getGruppenNummer() {
        return gruppenNummer;
    }

    public void setGruppenNummer(int gruppenNummer) {
        this.gruppenNummer = gruppenNummer;
    }

    public boolean isQualifying() {
        return qualifying;
    }

    public void setQualifying(boolean qualifying) {
        this.qualifying = qualifying;
    }

    public Set<UnterrichtsTag> getUnterrichtsTagSet() {
        return unterrichtsTagSet;
    }

    public void setUnterrichtsTagSet(UnterrichtsTag unterrichtsTag) {
        this.unterrichtsTagSet.add(unterrichtsTag);
    }

    public Set<TeilnehmerIn> getTeilnehmerInSet() {
        return TeilnehmerInSet;
    }

    public void setTeilnehmerInSet(TeilnehmerIn teilnehmer) {
        this.TeilnehmerInSet.add(teilnehmer);
    }

    public Raum getRaum() {
        return raum;
    }

    public void setRaum(Raum raum) {
        this.raum = raum;
    }
}


