package com.example.dashboard.entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

public abstract class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(name = "EMailAdresse", unique = true)
    private String eMailAdresse;

    @Column(name = "TelNummer", unique = true)
    private String telNummer;

    @Column(name = "Adresse", unique = false)
    private String adresse;

    //leerer konstruktor
    public Person() {}

    public Person(String name, String eMailAdresse, String telNummer, String adresse) {
        this.name = name;
        this.eMailAdresse = eMailAdresse;
        this.telNummer = telNummer;
        this.adresse = adresse;
    }

    //getter & setter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteMailAdresse() {
        return eMailAdresse;
    }

    public void seteMailAdresse(String eMailAdresse) {
        this.eMailAdresse = eMailAdresse;
    }

    public String getTelNummer() {
        return telNummer;
    }

    public void setTelNummer(String telNummer) {
        this.telNummer = telNummer;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", eMailAdresse='" + eMailAdresse + '\'' +
                ", telNummer='" + telNummer + '\'' +
                ", adresse='" + adresse + '\'' +
                '}';
    }
}
