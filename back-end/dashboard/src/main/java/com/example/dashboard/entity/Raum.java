package com.example.dashboard.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Raum {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private int raumNummer;

    @Column
    private String raumBezeichnung;

    @OneToOne()
    @JoinColumn(name = "gruppe_id", referencedColumnName = "id")
    private Gruppe gruppe;

    public Raum() {}

    public Raum(String raumBezeichnung, int raumNummer, Gruppe gruppe) {
        this.raumNummer = raumNummer;
        this.raumBezeichnung = raumBezeichnung;
        this.gruppe = gruppe;
    }

    public Raum(String raumBezeichnung, int raumNummer) {
        this.raumNummer = raumNummer;
        this.raumBezeichnung = raumBezeichnung;
    }

    //getter & setter


    public int getId() {
        return id;
    }

    public int getRaumNummer() {
        return raumNummer;
    }

    public void setRaumNummer(int raumNummer) {
        this.raumNummer = raumNummer;
    }

    public String getRaumBezeichnung() {
        return raumBezeichnung;
    }

    public void setRaumBezeichnung(String raumBezeichnung) {
        this.raumBezeichnung = raumBezeichnung;
    }

    public void setGruppe(Gruppe gruppe) {
        this.gruppe = gruppe;
    }

    public void deleteGruppe(Gruppe gruppe) {
        this.gruppe = null;
    }
}
