package com.example.dashboard.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class TeilnehmerIn extends Person {


    @Column
    private String startDatum;

    @Column
    private String endDatum;

    @ManyToOne
    @JsonBackReference(value = "teilnehmerGruppe")
    @JoinColumn(name="gruppe_id")
    private Gruppe gruppe;

    public TeilnehmerIn() {}

    public TeilnehmerIn(String name, String eMailAdresse, String telNummer, String adresse, String startDatum, String endDatum) {
        super(name, eMailAdresse, telNummer, adresse);
        this.startDatum = startDatum;
        this.endDatum = endDatum;
    }

    public TeilnehmerIn(String name, String eMailAdresse, String telNummer, String adresse, String startDatum, String endDatum, Gruppe gruppe) {
        super(name, eMailAdresse, telNummer, adresse);
        this.startDatum = startDatum;
        this.endDatum = endDatum;
        this.gruppe = gruppe;
    }



    //getter & setter


    public Gruppe getGruppe() {
        return gruppe;
    }

    public void setGruppe(Gruppe gruppe) {
        this.gruppe = gruppe;
    }

    public String getStartDatum() {
        return startDatum;
    }

    public void setStartDatum(String startDatum) {
        this.startDatum = startDatum;
    }

    public String getEndDatum() {
        return endDatum;
    }

    public void setEndDatum(String endDatum) {
        this.endDatum = endDatum;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" +"TeilnehmerIn{" +
                "startDatum='" + startDatum + '\'' +
                ", endDatum='" + endDatum + '\'' +
                ", gruppe=" + gruppe +
                '}';
    }
}