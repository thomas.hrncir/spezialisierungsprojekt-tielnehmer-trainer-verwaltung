package com.example.dashboard.entity;


import com.example.dashboard.enums.Anstellung;
import com.example.dashboard.enums.FachEnums;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "trainerIn")
public class TrainerIn extends Person{

    @Enumerated(EnumType.STRING)
    private Anstellung anstellung;

    @Column(nullable = false)
    private float bruttoGehalt;

    @Enumerated(EnumType.STRING)
    private FachEnums fachEnumsWissen;

    @OneToMany(mappedBy = "trainerIn")
    @JsonBackReference(value = "trainerUnterricht")
    private Set<UnterrichtsTag> unterrichtsTagSet = new HashSet<>();

    @ManyToOne
    @JsonBackReference(value = "fachTrainer")
    @JoinColumn(name="fach_id")
    private Fach fach;

    public TrainerIn() {}


    public TrainerIn(String name, String eMailAdresse, String telNummer, String adresse, Anstellung anstellung, float bruttoGehalt, FachEnums fachEnumsWissen) {
        super(name, eMailAdresse, telNummer, adresse);
        this.anstellung = anstellung;
        this.bruttoGehalt = bruttoGehalt;
        this.fachEnumsWissen = fachEnumsWissen;
    }

    public TrainerIn(String name, String eMailAdresse, String telNummer, String adresse, Anstellung anstellung, float bruttoGehalt, FachEnums fachEnumsWissen, Fach fach) {
        super(name, eMailAdresse, telNummer, adresse);
        this.anstellung = anstellung;
        this.bruttoGehalt = bruttoGehalt;
        this.fachEnumsWissen = fachEnumsWissen;
        this.fach = fach;
    }



    //geter & setter

    public void deleteUnterricht(UnterrichtsTag unterrichtsTag) {
        unterrichtsTagSet.remove(unterrichtsTag);
    }

    public Anstellung getAnstellung() {
        return anstellung;
    }

    public void setAnstellung(Anstellung anstellung) {
        this.anstellung = anstellung;
    }

    public float getBruttoGehalt() {
        return bruttoGehalt;
    }

    public void setBruttoGehalt(float bruttoGehalt) {
        this.bruttoGehalt = bruttoGehalt;
    }

    public FachEnums getFachEnumsWissen() {
        return fachEnumsWissen;
    }

    public void setFachEnumsWissen(FachEnums fachEnumsWissen) {
        this.fachEnumsWissen = fachEnumsWissen;
    }

    public Set<UnterrichtsTag> getUnterrichtsTagSet() {
        return unterrichtsTagSet;
    }

    public void setUnterrichtsTagSet(UnterrichtsTag unterrichtsTag) {
        this.unterrichtsTagSet.add(unterrichtsTag);
    }

    public Fach getFach() {
        return fach;
    }

    public void setFach(Fach fach) {
        this.fach = fach;
    }


}


