package com.example.dashboard.entity;

import com.example.dashboard.enums.TageEnums;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
public class UnterrichtsTag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    //TODO Json umderehen versuchen
    @ManyToOne
    @JsonManagedReference(value = "trainerUnterricht")
    @JoinColumn(name="trainerInId", referencedColumnName = "id")
    private TrainerIn trainerIn;

    @ManyToOne
    @JsonManagedReference(value ="gruppeUnterricht")
    @JoinColumn(name="gruppeId", referencedColumnName = "id")
    private Gruppe gruppe;

    @Enumerated(EnumType.STRING)
    private TageEnums tage;

    @ManyToOne
    @JsonManagedReference(value = "fachUnterricht")
    @JoinColumn(name="fachId", referencedColumnName = "id")
    private Fach fach;

    public UnterrichtsTag() {}

    public UnterrichtsTag(TrainerIn trainerIn, Gruppe gruppe, TageEnums tage, Fach fach) {
        this.trainerIn = trainerIn;
        this.gruppe = gruppe;
        this.tage = tage;
        this.fach = fach;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TrainerIn getTrainerIn() {
        return trainerIn;
    }

    public void setTrainerIn(TrainerIn trainerIn) {
        this.trainerIn = trainerIn;
    }

    public Gruppe getGruppe() {
        return gruppe;
    }

    public void setGruppe(Gruppe gruppe) {
        this.gruppe = gruppe;
    }

    public TageEnums getTage() {
        return tage;
    }

    public void setTage(TageEnums tage) {
        this.tage = tage;
    }

    public Fach getFach() {
        return fach;
    }

    public void setFach(Fach fach) {
        this.fach = fach;
    }
}
