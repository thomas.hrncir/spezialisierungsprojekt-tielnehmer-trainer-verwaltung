package com.example.dashboard.repository;

import com.example.dashboard.entity.Fach;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FachCRUDRepository extends CrudRepository<Fach, Integer> {
}
