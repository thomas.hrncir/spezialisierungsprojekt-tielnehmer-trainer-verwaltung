package com.example.dashboard.repository;

import com.example.dashboard.entity.Gruppe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GruppeCRUDRepository extends CrudRepository<Gruppe, Integer> {
}
