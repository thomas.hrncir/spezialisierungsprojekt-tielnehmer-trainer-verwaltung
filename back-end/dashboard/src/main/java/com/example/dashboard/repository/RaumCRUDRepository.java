package com.example.dashboard.repository;

import com.example.dashboard.entity.Raum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaumCRUDRepository extends CrudRepository<Raum, Integer> {
}
