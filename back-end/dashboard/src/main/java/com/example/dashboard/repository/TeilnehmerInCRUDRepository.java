package com.example.dashboard.repository;

import com.example.dashboard.entity.TeilnehmerIn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeilnehmerInCRUDRepository extends CrudRepository<TeilnehmerIn, Integer> {
}
