package com.example.dashboard.repository;

import com.example.dashboard.entity.TrainerIn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainerInCRUDRepository extends CrudRepository<TrainerIn, Integer> {
}
