package com.example.dashboard.repository;

import com.example.dashboard.entity.UnterrichtsTag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnterrichtsTagCRUDRepository extends CrudRepository<UnterrichtsTag, Integer> {
}
