package com.example.dashboard.services;


import com.example.dashboard.entity.Fach;
import com.example.dashboard.repository.FachCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FachService {

    @Autowired
    FachCRUDRepository fachCRUDRepository;

    public List<Fach> getAllFaecher() {
        return  (List<Fach>) fachCRUDRepository.findAll();
    }

    public Integer postFach(Fach fach) {
        return fachCRUDRepository.save(fach).getId();
    }

    public void putFach(Fach fach, int id) {
        Fach dbFach = fachCRUDRepository.findById(id).get();

        if(fach.getFachBezeichnung() != null) {
            dbFach.setFachBezeichnung(fach.getFachBezeichnung());
        }

        if(fach.getFachUnterricht() != null) {
            dbFach.setFachUnterricht(fach.getFachUnterricht());
        }

        fachCRUDRepository.save(dbFach);
    }

    public void deleteFach(int fachid) {
        fachCRUDRepository.deleteById(fachid);
    }

}
