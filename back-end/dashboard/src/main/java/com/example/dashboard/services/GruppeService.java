package com.example.dashboard.services;


import com.example.dashboard.dtos.GruppeDTO;
import com.example.dashboard.dtos.TeilnehmerDTO;
import com.example.dashboard.dtos.UebersichtDTO;
import com.example.dashboard.entity.Gruppe;
import com.example.dashboard.entity.Raum;
import com.example.dashboard.entity.TeilnehmerIn;
import com.example.dashboard.entity.UnterrichtsTag;
import com.example.dashboard.enums.GruppeEnums;
import com.example.dashboard.enums.TageEnums;
import com.example.dashboard.repository.GruppeCRUDRepository;
import com.example.dashboard.repository.RaumCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class GruppeService {

    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;

    @Autowired
    RaumCRUDRepository raumCRUDRepository;

    public Integer postGruppe(Gruppe gruppe) {
        return gruppeCRUDRepository.save(gruppe).getId();
    }

    public List<Gruppe> getAllGruppe() {
        return (List<Gruppe>) gruppeCRUDRepository.findAll();
    }

    public void setGruppe(GruppeEnums gruppeEnums, Integer gruppenNummer, Boolean qualifying, Integer raumId) {

        Raum raum = raumCRUDRepository.findById(raumId).get();

        Gruppe gruppe = new Gruppe(gruppeEnums, gruppenNummer, qualifying, raum);

        raum.setGruppe(gruppe);

        gruppeCRUDRepository.save(gruppe);
        raumCRUDRepository.save(raum);
    }

    public void putGruppe(GruppeDTO gruppeDTO, int id) {
        Gruppe dbGruppe = gruppeCRUDRepository.findById(id).get();

        if (gruppeDTO.getGruppenNummer() != 0) {
            dbGruppe.setGruppenNummer(gruppeDTO.getGruppenNummer());
        }

        if (gruppeDTO.getArtDerAusbildung() != null) {
            dbGruppe.setArtDerAusbildung(gruppeDTO.getArtDerAusbildung());
        }
        //TODO boolean if löschen
        Boolean bool = gruppeDTO.isQualifying();

        if (bool == true || bool == false) {
            dbGruppe.setQualifying(gruppeDTO.isQualifying());
        }

        if(gruppeDTO.getRaumId() != 0) {
            if(dbGruppe.getRaum() != null){
            dbGruppe.getRaum().deleteGruppe(dbGruppe);
            }
            Raum raum = raumCRUDRepository.findById(gruppeDTO.getRaumId()).get();
            dbGruppe.setRaum(raum);
            raum.setGruppe(dbGruppe);
            raumCRUDRepository.save(raum);
        }

        gruppeCRUDRepository.save(dbGruppe);
    }

    public void deleteGruppe(int gruppeid) {

        Gruppe gruppe = gruppeCRUDRepository.findById(gruppeid).get();
        Raum raum = gruppe.getRaum();
        raum.setGruppe(null);
        raumCRUDRepository.save(raum);
        gruppeCRUDRepository.deleteById(gruppeid);
    }

    public UebersichtDTO kompletteUebersicht(int gruppeId) {

        Set<UnterrichtsTag> unterrichtsTag = gruppeCRUDRepository.findById(gruppeId).get().getUnterrichtsTagSet();
        Set<TeilnehmerIn> teilnehmerSet = gruppeCRUDRepository.findById(gruppeId).get().getTeilnehmerInSet();

        return new UebersichtDTO(unterrichtsTag, teilnehmerSet);
    }

}
