package com.example.dashboard.services;


import com.example.dashboard.entity.Gruppe;
import com.example.dashboard.entity.Raum;
import com.example.dashboard.repository.GruppeCRUDRepository;
import com.example.dashboard.repository.RaumCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Service
public class RaumService {

    @Autowired
    RaumCRUDRepository raumCRUDRepository;

    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;


    public Integer postRaum(Raum raum) {
        return raumCRUDRepository.save(raum).getId();
    }

    public List<Raum> getAllRaeume() {
        return (List<Raum>) raumCRUDRepository.findAll();
    }

    public void putRaum(Raum raum, int id) {
        Raum dbRaum = raumCRUDRepository.findById(id).get();

        Integer raumNummer = raum.getRaumNummer();
        if(raumNummer != 0) {
            dbRaum.setRaumNummer(raum.getRaumNummer());
        }

        if(raum.getRaumBezeichnung() != null) {
            dbRaum.setRaumBezeichnung(raum.getRaumBezeichnung());
        }
        raumCRUDRepository.save(dbRaum);
    }

    public void deleteRaum(int raumid) {
        raumCRUDRepository.deleteById(raumid);
    }

    public void setRaum(String raumBezeichnung, int raumNummer) {

        //Gruppe gruppe = gruppeCRUDRepository.findById(gruppeId).get();

        Raum raum = new Raum(raumBezeichnung, raumNummer);

        //gruppe.setRaum(raum);

        raumCRUDRepository.save(raum);
        //gruppeCRUDRepository.save(gruppe);
    }

}
