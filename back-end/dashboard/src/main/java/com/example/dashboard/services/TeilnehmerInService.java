package com.example.dashboard.services;


import com.example.dashboard.dtos.TeilnehmerDTO;
import com.example.dashboard.entity.Gruppe;
import com.example.dashboard.entity.TeilnehmerIn;
import com.example.dashboard.entity.TrainerIn;
import com.example.dashboard.repository.GruppeCRUDRepository;
import com.example.dashboard.repository.TeilnehmerInCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class TeilnehmerInService {

    @Autowired
    TeilnehmerInCRUDRepository teilnehmerInCRUDRepository;

    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;

    public Integer post(TeilnehmerIn teilnehmerIn) {
        return teilnehmerInCRUDRepository.save(teilnehmerIn).getId();
    }

    public TeilnehmerIn getTeilnehmerById(int teilnehmerId) {
        return (TeilnehmerIn) teilnehmerInCRUDRepository.findById(teilnehmerId).get();
    }

    public void setTeilnehmer(String name, String eMailAdresse, String telNummer, String adresse, String startDatum, String endDatum, Integer gruppeId) {

        Gruppe gruppe = gruppeCRUDRepository.findById(gruppeId).get();

        TeilnehmerIn teilnehmer = new TeilnehmerIn(name, eMailAdresse, telNummer, adresse, startDatum, endDatum, gruppe);

        gruppe.setTeilnehmerInSet(teilnehmer);

        teilnehmerInCRUDRepository.save(teilnehmer);
        gruppeCRUDRepository.save(gruppe);
    }

    public List<TeilnehmerIn> getAllTeilnehmer() {
        return (List<TeilnehmerIn>) teilnehmerInCRUDRepository.findAll();
    }

    public void putTeilnehmerIn(TeilnehmerDTO teilnehmerDTO, int id) {
        TeilnehmerIn dbTeilnehmerIn = teilnehmerInCRUDRepository.findById(id).get();
        System.out.println(dbTeilnehmerIn.toString());

        if(teilnehmerDTO.getAdresse() != null) {
            dbTeilnehmerIn.setAdresse(teilnehmerDTO.getAdresse());
        }

        if(teilnehmerDTO.geteMailAdresse() != null) {
            dbTeilnehmerIn.seteMailAdresse(teilnehmerDTO.geteMailAdresse());
        }

        if(teilnehmerDTO.getName() != null) {
            dbTeilnehmerIn.setName(teilnehmerDTO.getName());
        }

        if(teilnehmerDTO.getTelNummer() != null) {
            dbTeilnehmerIn.setTelNummer(teilnehmerDTO.getTelNummer());
        }

        if(teilnehmerDTO.getEndDatum() != null) {
            dbTeilnehmerIn.setEndDatum(teilnehmerDTO.getEndDatum());
        }

        if(teilnehmerDTO.getStartDatum() != null) {
            dbTeilnehmerIn.setStartDatum(teilnehmerDTO.getStartDatum());
        }

        if(teilnehmerDTO.getGruppeId() != 0) {
            dbTeilnehmerIn.getGruppe().deleteTeilnehmer(dbTeilnehmerIn);
            Gruppe gruppe = gruppeCRUDRepository.findById(teilnehmerDTO.getGruppeId()).get();
            dbTeilnehmerIn.setGruppe(gruppe);
            gruppe.setTeilnehmerInSet(dbTeilnehmerIn);
            gruppeCRUDRepository.save(gruppe);
        }

        teilnehmerInCRUDRepository.save(dbTeilnehmerIn);
    }

    public void deleteTeilnehmer(int trainerid) {
        teilnehmerInCRUDRepository.deleteById(trainerid);
    }
}
