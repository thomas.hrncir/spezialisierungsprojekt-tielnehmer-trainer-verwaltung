package com.example.dashboard.services;

import com.example.dashboard.dtos.GetTrainerInDTO;
import com.example.dashboard.dtos.TrainerInDTO;
import com.example.dashboard.entity.Fach;
import com.example.dashboard.entity.TeilnehmerIn;
import com.example.dashboard.entity.TrainerIn;
import com.example.dashboard.enums.Anstellung;
import com.example.dashboard.enums.FachEnums;
import com.example.dashboard.repository.FachCRUDRepository;
import com.example.dashboard.repository.TrainerInCRUDRepository;
import com.sun.xml.bind.v2.TODO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrainerInService {

    @Autowired
    TrainerInCRUDRepository trainerInCRUDRepository;

    @Autowired
    FachCRUDRepository fachCRUDRepository;

    public float calculateNettoSalary(float bruttoGehalt) {
        //TrainerIn trainerIn = trainerInCRUDRepository.findById(trainerId).get();

        //double nettoSalary = trainerIn.getBruttoGehalt() * 0.8;
        float nettoSalary = (float) (bruttoGehalt * 0.8);

        return nettoSalary;
    }

    public void post(TrainerIn trainerIn) {
        trainerInCRUDRepository.save(trainerIn);
    }

    public void setTrainer(String name, String eMailAdresse, String telNummer, String adresse, Anstellung anstellung, Float bruttoGehalt, FachEnums fachEnums, Integer fachId) {

        Fach fach = fachCRUDRepository.findById(fachId).get();

        TrainerIn trainerIn = new TrainerIn(name, eMailAdresse, telNummer, adresse, anstellung, bruttoGehalt, fachEnums, fach);

        fach.setTrainerInSet(trainerIn);

        trainerInCRUDRepository.save(trainerIn);
        fachCRUDRepository.save(fach);
    }


    public Integer postId(TrainerIn trainerIn) {
        return trainerInCRUDRepository.save(trainerIn).getId();
    }

    public List<TrainerIn> getAllTrainer() {
        return (List<TrainerIn>) trainerInCRUDRepository.findAll();
    }

    public List<GetTrainerInDTO> getAllTrainerDTO() {
        List<GetTrainerInDTO> GetAllTrainerDTOS = new ArrayList<>();
        for(TrainerIn trainerIn : trainerInCRUDRepository.findAll()) {
            GetAllTrainerDTOS.add(new GetTrainerInDTO(trainerIn.getId(), trainerIn.getName(), trainerIn.geteMailAdresse(), trainerIn.getTelNummer(), trainerIn.getAdresse(), trainerIn.getAnstellung(), trainerIn.getBruttoGehalt(), this.calculateNettoSalary(trainerIn.getBruttoGehalt()), trainerIn.getFachEnumsWissen()));
        }
        return GetAllTrainerDTOS;
    }

    public TrainerIn getTrainerById(int trainerId) {
        return (TrainerIn) trainerInCRUDRepository.findById(trainerId).get();
    }

    public void putTrainerIn(TrainerInDTO trainerInDTO, int id) {
        TrainerIn dbTrainerIn = trainerInCRUDRepository.findById(id).get();

        if(trainerInDTO.getAdresse() != null) {
            dbTrainerIn.setAdresse(trainerInDTO.getAdresse());
        }

        if(trainerInDTO.geteMailAdresse() != null) {
            dbTrainerIn.seteMailAdresse(trainerInDTO.geteMailAdresse());
        }

        if(trainerInDTO.getName() != null) {
            dbTrainerIn.setName(trainerInDTO.getName());
        }

        if(trainerInDTO.getTelNummer() != null) {
            dbTrainerIn.setTelNummer(trainerInDTO.getTelNummer());
        }

        if(trainerInDTO.getAnstellung() != null) {
            dbTrainerIn.setAnstellung(trainerInDTO.getAnstellung());
        }

        if(trainerInDTO.getBruttoGehalt() != 0) {
            dbTrainerIn.setBruttoGehalt(trainerInDTO.getBruttoGehalt());
        }

       // if(trainerInDTO.getFachEnums() != null) {
            dbTrainerIn.setFachEnumsWissen(trainerInDTO.getFachEnums());
        //}

        if(trainerInDTO.getFachId() != 0) {
            dbTrainerIn.getFach().deleteTrainer(dbTrainerIn);
            Fach fach = fachCRUDRepository.findById(trainerInDTO.getFachId()).get();
            dbTrainerIn.setFach(fach);
            fach.setTrainerInSet(dbTrainerIn);
            fachCRUDRepository.save(fach);
        }
        trainerInCRUDRepository.save(dbTrainerIn);

    }

        public void deleteTrainer(int trainerid) {
            trainerInCRUDRepository.deleteById(trainerid);
        }


}


