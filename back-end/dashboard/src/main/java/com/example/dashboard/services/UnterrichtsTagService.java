package com.example.dashboard.services;


import com.example.dashboard.dtos.UnterrichtsTagDTO;
import com.example.dashboard.entity.Fach;
import com.example.dashboard.entity.Gruppe;
import com.example.dashboard.entity.TrainerIn;
import com.example.dashboard.entity.UnterrichtsTag;
import com.example.dashboard.enums.TageEnums;
import com.example.dashboard.repository.FachCRUDRepository;
import com.example.dashboard.repository.GruppeCRUDRepository;
import com.example.dashboard.repository.TrainerInCRUDRepository;
import com.example.dashboard.repository.UnterrichtsTagCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnterrichtsTagService {

    @Autowired
    UnterrichtsTagCRUDRepository unterrichtsTagCRUDRepository;

    @Autowired
    TrainerInCRUDRepository trainerInCRUDRepository;

    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;

    @Autowired
    FachCRUDRepository fachCRUDRepository;

    public Integer post(UnterrichtsTag unterrichtsTag) {
        return unterrichtsTagCRUDRepository.save(unterrichtsTag).getId();
    }

    public void setUnterricht(Integer trainerId, Integer gruppeId, TageEnums tageEnums, Integer fachId) {

        //Objekte aus Datenbank holen
        TrainerIn trainerIn = trainerInCRUDRepository.findById(trainerId).get();
        Gruppe gruppe = gruppeCRUDRepository.findById(gruppeId).get();
        Fach fach = fachCRUDRepository.findById(fachId).get();

        //Neuen Unterricht erstellen
        UnterrichtsTag unterrichtsTag = new UnterrichtsTag(trainerIn, gruppe, tageEnums, fach);

        //Obekte im Backaned updaten
        trainerIn.setUnterrichtsTagSet(unterrichtsTag);
        gruppe.setUnterrichtsTagSet(unterrichtsTag);
        fach.setUnterrichtsTagSet(unterrichtsTag);

        //DB update
        unterrichtsTagCRUDRepository.save(unterrichtsTag);
        trainerInCRUDRepository.save(trainerIn);
        fachCRUDRepository.save(fach);
        gruppeCRUDRepository.save(gruppe);

    }


    public List<UnterrichtsTag> getAllUnterrichtsTage() {
        return (List<UnterrichtsTag>) unterrichtsTagCRUDRepository.findAll();
    }

    public void putUnterrichtsTag(UnterrichtsTagDTO unterrichtsTagDTO, int id) {
        UnterrichtsTag dbUnterrichtsTag = unterrichtsTagCRUDRepository.findById(id).get();


        if(unterrichtsTagDTO.getTageEnums() != null) {
            dbUnterrichtsTag.setTage(unterrichtsTagDTO.getTageEnums());
        }//TODO if statemens für gejointe klassen
        if(unterrichtsTagDTO.getFachId() != 0) {
            dbUnterrichtsTag.getFach().deleteUnterricht(dbUnterrichtsTag);
            Fach fach = fachCRUDRepository.findById(unterrichtsTagDTO.getFachId()).get();
            dbUnterrichtsTag.setFach(fach);
            fach.setUnterrichtsTagSet(dbUnterrichtsTag);
            fachCRUDRepository.save(fach);
        }
        if(unterrichtsTagDTO.getGruppeId() != 0) {
            dbUnterrichtsTag.getGruppe().deleteUnterricht(dbUnterrichtsTag);
            Gruppe gruppe = gruppeCRUDRepository.findById(unterrichtsTagDTO.getGruppeId()).get();
            dbUnterrichtsTag.setGruppe(gruppe);
            gruppe.setUnterrichtsTagSet(dbUnterrichtsTag);
            gruppeCRUDRepository.save(gruppe);
        }
        if(unterrichtsTagDTO.getTrainerId() != 0) {
            dbUnterrichtsTag.getTrainerIn().deleteUnterricht(dbUnterrichtsTag);
            TrainerIn trainerIn = trainerInCRUDRepository.findById(unterrichtsTagDTO.getTrainerId()).get();
            dbUnterrichtsTag.setTrainerIn(trainerIn);
            trainerIn.setUnterrichtsTagSet(dbUnterrichtsTag);
            trainerInCRUDRepository.save(trainerIn);
        }

        unterrichtsTagCRUDRepository.save(dbUnterrichtsTag);

    }

    public void deleteUnterrichtsTag(int unterrichtsTagId) {
        unterrichtsTagCRUDRepository.deleteById(unterrichtsTagId);
    }

}
