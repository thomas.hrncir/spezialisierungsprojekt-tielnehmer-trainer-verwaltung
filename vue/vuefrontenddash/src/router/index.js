import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/HomeSite.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },

    {
        path: '/teilnehmer',
        name: 'Teilnehmer',
        component: () => import('../components/TeilnehmerSeite.vue')
    },

    {
        path: '/trainer',
        name: 'Trainer',
        component: () => import('../components/TrainerSeite.vue')
    
    },

    {
        path: '/gruppe',
        name: 'Gruppen',
        component: () => import('../components/GruppenSeite.vue')
    },

    {
        path: '/raum',
        name: 'Raeume',
        component: () => import('../components/RaumSeite.vue')
    },

    {
        path: '/fach',
        name: 'Faecher',
        component: () => import('../components/FachSeite.vue')    
    },

    {
        path: '/unterrichtstag',
        name: 'Unterrichtstage',
        component: () => import('../components/UnterrichtSeite.vue') 
    }

]

const router = new VueRouter({
    routes
})

export default router