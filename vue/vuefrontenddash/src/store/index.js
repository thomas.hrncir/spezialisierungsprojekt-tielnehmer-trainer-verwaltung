import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        alleTeilnehmer: [],
        teilnehmerById: '',
        
        alleTrainer: [],
        trainerByIdNetto: '',

        alleGruppen: [],

        alleRaeume: [],

        alleFaecher: [],

        alleUnterrichtsTage: [],

        alleUnterrichtsTageByGruppeId: [],
    },
    mutations:{
        setAlleTeilnehmer(state, alleTeilnehmer) {
            state.alleTeilnehmer = alleTeilnehmer;
        },

        setTeilnehmerById(state, teilnehmerById) {
            state.teilnehmerById = teilnehmerById;
        },

        setAlleTrainer(state, alleTrainer) {
            state.alleTrainer = alleTrainer;
        },

        setTrainerNettoGehalt(state, trainerByIdNetto) {
            state.trainerByIdNetto = trainerByIdNetto;
        },

        setAlleGruppen(state, alleGruppen) {
            state.alleGruppen = alleGruppen;
        },

        setAlleRaeume(state, alleRaeume) {
            state.alleRaeume = alleRaeume;
        },

        setAlleFaecher(state, alleFaecher) {
            state.alleFaecher = alleFaecher;
        },

        setAlleUnterrichtsTage(state, alleUnterrichtsTage) {
            state.alleUnterrichtsTage = alleUnterrichtsTage;
        },

        setAlleUnterrichtsTageByGruppeId(state, alleUnterrichtsTageByGruppeId) {
            state.alleUnterrichtsTageByGruppeId = alleUnterrichtsTageByGruppeId;
        }
    },
    actions:{
    
        async getAlleTeilnehmer(context) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/teilnehmerIn/allTeilnehmer");
            console.log(response)
            context.commit('setAlleTeilnehmer', response.data);
        },
        
        async deleteTeilnehmer(context, id) {
            await Vue.axios.delete("http://127.0.0.1:8080/teilnehmerIn/"+id)
            context.dispatch('getAlleTeilnehmer');
        },

        async postTeilnehmer(context, teilnehmer) {
            console.warn(teilnehmer)
            await Vue.axios.post("http://127.0.0.1:8080/teilnehmerIn/dtos", teilnehmer)
            context.dispatch('getAlleTeilnehmer');
        },

        async getTeilnehmerById(context, id) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/teilnehmerIn/byid/"+id)
            console.log(response)
        },

        async putTeilnehmer(context, teilnehmerUpdate) {
            console.warn(teilnehmerUpdate)
            const response = await Vue.axios.put("http://127.0.0.1:8080/teilnehmerIn/" + teilnehmerUpdate.id, teilnehmerUpdate)
            console.log(response)
            context.dispatch('getAlleTeilnehmer');
        },


        async getAlleTrainer(context) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/trainerIn/allTrainerDTO");
            console.log(response)
            context.commit('setAlleTrainer', response.data);
        },

        async getTrainerNettoGehalt(context, id) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/trainerIn/gehalt/" + id)
            context.commit('setTrainerNettoGehalt', response.data)
        },

        async deleteTrainer(context, id) {
            await Vue.axios.delete("http://127.0.0.1:8080/trainerIn/" + id)
            context.dispatch('getAlleTrainer');
        },

        async postTrainer(context, trainer) {
            await Vue.axios.post("http://127.0.0.1:8080/trainerIn/dtos", trainer)
            context.dispatch('getAlleTrainer');
        },

        async putTrainer(context, trainerUpdate) {
            await Vue.axios.put("http://127.0.0.1:8080/trainerIn/" + trainerUpdate.id, trainerUpdate)
            context.dispatch('getAlleTrainer');
        },


        async getAlleGruppen(context) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/gruppe");
            console.log(response)
            context.commit('setAlleGruppen', response.data)

        },

        async deleteGruppe(context, id) {
            await Vue.axios.delete("http://127.0.0.1:8080/gruppe/" + id)
            context.dispatch('getAlleGruppen');
        },

        async postGruppe(context, gruppe) {
            await Vue.axios.post("http://127.0.0.1:8080/gruppe/dtos", gruppe)
            console.log(gruppe)
            context.dispatch('getAlleGruppen');
        },

        async putGruppe(context, gruppeUpdate) {
            await Vue.axios.put("http://127.0.0.1:8080/gruppe/" + gruppeUpdate.id, gruppeUpdate)
            console.log(gruppeUpdate)
            context.dispatch('getAlleGruppen');
        },


        async getAlleRaeume(context) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/raum");
            console.log(response)
            context.commit('setAlleRaeume', response.data)
        },

        async deleteRaum(context, id) {
            await Vue.axios.delete("http://127.0.0.1:8080/raum/" + id)
            context.dispatch('getAlleRaeume');
        },

        async postRaum(context, raum) {
            await Vue.axios.post("http://127.0.0.1:8080/raum/dtos", raum)
            context.dispatch('getAlleRaeume');
        },

        async putRaum(context, raumUpdate) {
            await Vue.axios.put("http://127.0.0.1:8080/raum/" + raumUpdate.id, raumUpdate)
            console.log(raumUpdate)
            context.dispatch('getAlleRaeume');
        },


        async getAlleFaecher(context) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/fach/allFaecher");
            console.log(response)
            context.commit('setAlleFaecher', response.data);
        },

        async deleteFach(context, id) {
            await Vue.axios.delete("http://127.0.0.1:8080/fach/" + id)
            context.dispatch('getAlleFaecher');
        },

        async postFach(context, fach) {
            await Vue.axios.post("http://127.0.0.1:8080/fachReturn/", fach)
            context.dispatch('getAlleFaecher');
        },

        async putFach(context, fachUpdate) {
            await Vue.axios.put("http://127.0.0.1:8080/fach/" + fachUpdate.id, fachUpdate)
            console.log(fachUpdate)
            context.dispatch('getAlleFaecher');
        },


        async getAlleUnterrichtsTage(context) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/unterrichtsTag/");
            console.log(response)
            context.commit('setAlleUnterrichtsTage', response.data);
        },

        async deleteUnterrichtsTag(context, id) {
            await Vue.axios.delete("http://127.0.0.1:8080/unterrichtsTag/" + id)
            context.dispatch('getAlleUnterrichtsTage');
        },

        async postUnterrichtsTag(context, unterrichtsTag) {
            await Vue.axios.post("http://127.0.0.1:8080/unterrichtsTag/dtos", unterrichtsTag)
            context.dispatch('getAlleUnterrichtsTage');
        },

        async putUnterrichtsTag(context, unterrichtsTagUpdate) {
            await Vue.axios.put("http://127.0.0.1:8080/unterrichtsTag/" + unterrichtsTagUpdate.id, unterrichtsTagUpdate)
            console.log(unterrichtsTagUpdate)
            context.dispatch('getAlleUnterrichtsTage');
        },


        async getAlleUnterrichtsTageByGruppeId(context, id) {
            const response = await Vue.axios.get("http://127.0.0.1:8080/gruppe/uebersicht/" + id)
            console.warn(response)
            context.commit('setAlleUnterrichtsTageByGruppeId', response.data);
        }
 

    }
})


